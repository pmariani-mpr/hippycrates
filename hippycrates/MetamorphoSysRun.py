import os
import numpy as np
import pandas as pd
from hippycrates import config as conf
from hippycrates import constants as const
import logging

logging.basicConfig(level=conf.LOG_LEVEL, format=conf.LOG_FMT)
logger = logging.getLogger(__name__)


def wrap_pandas_read(full_path, column_names, iterator, dtype=None):

    # RRF files seem to be a pipe delimited file, with an extra pipe at the end
    # which gets read as an empty column, this logic gets rid of that
    if column_names is not None:
        if column_names[-1] != const.RRF_EMPTY_COL_NAME:
            column_names.append(const.RRF_EMPTY_COL_NAME)

    out = pd.read_csv(full_path,
                      sep='|',
                      header=None,
                      engine='c',
                      usecols=lambda x: x != const.RRF_EMPTY_COL_NAME,
                      names=column_names,
                      dtype=dtype,
                      iterator=iterator)

    # Forcing the columns names to be strings, even when we don't have don't have column names
    out.columns = out.columns.astype(str)

    return out


def get_file_metadata_dict(rrf_file, files_metadata_df):

    FILE_COL = 'FIL'

    if all([colname.islower() for colname in files_metadata_df.columns]):
        FILE_COL = FILE_COL.lower()

    matches = np.nonzero(
        files_metadata_df[FILE_COL].apply(lambda x: x == rrf_file))[0]

    meta_dict = {}
    if len(matches) == 0:
        logger.info('Unable to get metadata for {}'.format(rrf_file))
    elif len(matches) == 1:
        meta_dict = files_metadata_df.iloc[matches[0]].to_dict()
    else:
        raise(ValueError("Serveral Rows {} match {}".format(matches, rrf_file)))
    return meta_dict


def get_file_data_dictionary(rrf_file, data_dictionary_df):

    file_col = 'FIL'

    if all([colname.islower() for colname in data_dictionary_df.columns]):
        file_col = file_col.lower()


    return data_dictionary_df.loc[data_dictionary_df[file_col] == rrf_file].copy()


def get_file_type_dict(data_dictionary_df):

    dty_col = 'DTY'
    col_col = 'COL'

    if all([colname.islower() for colname in data_dictionary_df.columns]):
        dty_col = dty_col.lower()
        col_col = col_col.lower()

    data_dictionary_df['dty_base'] = data_dictionary_df[dty_col].apply(
        lambda x: x.split('(')[0])

    dtype_dict = {}
    for idx, row in data_dictionary_df.iterrows():
        dtype_dict[row[col_col]] = conf.SQL_92_TO_PYTHON_DICT[row['dty_base']]

    return dtype_dict


def read_rrf_file(rrf_full_path,
                  files_metadata_df=None,
                  data_dictionary_df=None,
                  column_names=None,
                  iterator=False):

    rrf_file = os.path.basename(rrf_full_path)

    FMT_COL = 'FMT'
    if all([colname.islower() for colname in files_metadata_df.columns]):
        FMT_COL = FMT_COL.lower()

    if (files_metadata_df is not None) and (column_names is None):
        logger.debug("Getting Column Names From Metadata Files")
        meta_dict = get_file_metadata_dict(rrf_file, files_metadata_df)
        logger.debug("Metadata Dict:\n{}\n".format(meta_dict))

        if FMT_COL in meta_dict:
            column_names = meta_dict[FMT_COL].split(',')

    dtype_dict = None
    if (data_dictionary_df is not None):
        logger.debug("Getting Data Types from Data Dictionary")
        this_file_dictionary_df = get_file_data_dictionary(
            rrf_file, data_dictionary_df)
        dtype_dict = get_file_type_dict(this_file_dictionary_df)
        logger.debug("Dtype Dict:\n{}\n".format(dtype_dict))

    out = wrap_pandas_read(rrf_full_path, column_names,
                           iterator, dtype=dtype_dict)

    return out


class MetamorphoSysRun(object):
    def __init__(self, metamorphosys_install_dir,
                 umls_release,
                 rrf_folder='META',
                 lowercase_columns_names=True):

        self.metamorphosys_install_dir = metamorphosys_install_dir
        self.umls_release = umls_release
        self.rrf_path = os.path.join(
            metamorphosys_install_dir, umls_release, rrf_folder)

        self.lowercase_columns_names = lowercase_columns_names

        meta_headers = const.MRFILES_HEADERS

        if lowercase_columns_names:
            meta_headers = [name.lower() for name in meta_headers]

        files_meta_df = wrap_pandas_read(os.path.join(
            self.rrf_path, const.MRFILES_NAME), meta_headers, False)

        if lowercase_columns_names:
            files_meta_df['fmt'] = files_meta_df['fmt'].apply(
                lambda x: x.lower())

        self.files_metadata_df = files_meta_df

        data_dictionary_df = read_rrf_file(os.path.join(self.rrf_path, const.MRCOLS_NAME),
                                           files_metadata_df=self.files_metadata_df)

        if lowercase_columns_names:
            data_dictionary_df['col'] = data_dictionary_df['col'].apply(
                lambda x: x.lower())

        self.data_dictionary_df = data_dictionary_df

    def get_run_rrf_metadata(self, rrf_file):
        return get_file_metadata_dict(rrf_file, self.files_metadata_df)

    def describe_run_rrf_file(self, rrf_file):
        file_dict = self.get_run_rrf_metadata(rrf_file)
        data_dictionary = get_file_data_dictionary(rrf_file,
                                                   self.data_dictionary_df,
                                                   self.lowercase_columns_names)

        logger.info("File: \n{}\n".format(file_dict))
        logger.info("Data: \n{}\n".format(data_dictionary))

    def read_run_rrf_file(self, rrf_file, iterator=False):

        full_file = os.path.join(self.rrf_path, rrf_file)
        if os.path.isfile(full_file):
            return read_rrf_file(full_file,
                                 files_metadata_df=self.files_metadata_df.copy(),
                                 data_dictionary_df=self.data_dictionary_df.copy(),
                                 iterator=iterator)
        else:
            logger.info("No Such File, Skipping {}".format(rrf_file))
            return None
