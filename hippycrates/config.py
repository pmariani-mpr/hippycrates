import logging
import numpy as np

LOG_LEVEL = logging.INFO
LOG_FMT = '%(asctime)s|%(name)s|%(levelname)s|%(message)s'

# Integers are mapped to floats to better handle NAN values
SQL_92_TO_PYTHON_DICT = {'char':np.str,
                         'integer':np.float64,
                         'numeric':np.float64,
                         'varchar':np.str}

# Headers taken from the link below
# https://www.nlm.nih.gov/research/umls/new_users/online_learning/Meta_006.html

