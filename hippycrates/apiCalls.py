import requests
import xmltodict
import logging

from hippycrates import config as conf
from hippycrates import constants as const

logging.basicConfig(level=conf.LOG_LEVEL, format=conf.LOG_FMT)
logger = logging.getLogger(__name__)

# Order of this list matters
meaningless_keys = ['rxnormdata','rxclassdata']


def rxnav_api_call(endpoint, paramaters, remove_meaningless_keys=True):
    url = [const.URL_RXNAV_REST, endpoint]
    url = '/'.join(url)
    response = requests.get(url,paramaters)
    out = xmltodict.parse(response.text)


    if remove_meaningless_keys:
        for key in meaningless_keys:
            if key in out:
                assert len(out.keys())==1
                out = out[key]



    return out




def rx_approximate_term_search(search_term):

    url = [const.URL_RXNAV_REST, 'approximateTerm']
    url = '/'.join(url)
    logger.debug(url)

    response = requests.get(url,{'term':search_term})

    out = xmltodict.parse(response.text)

    assert len(out.keys())==1
    out = out['rxnormdata']
    assert len(out.keys())==1
    out = out['approximateGroup']

    comment = out['comment']

    logger.info("comment: \n{}\n".format(comment))

    out = out['candidate']

    return out, comment

def rx_cui_class_loopup(rx_cui,relaSource=None,relas=None):

    url = [const.URL_RXNAV_REST, 'rxclass/class/byRxcui']
    url = '/'.join(url)
    logger.debug(url)

    response = requests.get(url,{'rxcui':rx_cui,
                                 'relaSource':relaSource,
                                 'relas':relas})

    out = xmltodict.parse(response.text)

    assert len(out.keys())==1
    out = out['rxclassdata']

    assert len(out.keys())==2
    out = out['rxclassDrugInfoList']

    assert len(out.keys())==1
    out = out['rxclassDrugInfo']

    return out

def open_fda_call():
    response = requests.get(open_fda_url,{'search':'openfda.rxcui:"{}"'.format('1049565')})
    response.status_code
    out_dict = response.json()
    data_dict = out_dict['results'][0]
    info_list = list(data_dict.keys())
    n_buckets = int(np.ceil(len(info_list)/gap))