


#------------------------------------------------------
# 
# -----------------------------------------------------
# The documentation for the rich release format contains this information
# https://www.ncbi.nlm.nih.gov/books/NBK9685/
# 3.3.1. Files (File = MRFILES.RRF)

MRFILES_NAME = 'MRFILES.RRF'
# MRFILES_HEADERS_DESC = ['filename','description','column_names','n_cols','n_rows','filesize_bytes']
MRFILES_HEADERS =   [     'FIL',        'DES',         'FMT',   'CLS',   'RWS',           'BTS'] # This are the same names as those 

MRCOLS_NAME = 'MRCOLS.RRF'

#------------------------------------------------------
# Rest API Constants
# -----------------------------------------------------
URL_UMLS_API_AUTH = 'https://utslogin.nlm.nih.gov/cas/v1/api-key'
URL_UMLS_API_REST = 'https://uts-ws.nlm.nih.gov/rest'

URL_RXNAV_REST = 'https://rxnav.nlm.nih.gov/REST'

ULR_OPEN_FDA = 'https://api.fda.gov'

#------------------------------------------------------
# Misc
# -----------------------------------------------------

RRF_EMPTY_COL_NAME = 'blank'