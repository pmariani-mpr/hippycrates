from setuptools import setup, find_packages


setup(name='hippycrates',
      version='0.0.1',
      author="Peter Mariani",
      url='',
      author_email="pmariani@mathematica-mpr.com",
      description="A simple package to demo testing",
      packages=find_packages(exclude=['tests','examples','notebooks']),
      install_requires=[] );